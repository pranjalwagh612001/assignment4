provider "aws" {
  region = "us-east-1"  
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow-ssh"
  description = "Allow SSH inbound traffic"
  
  // Inbound rule for SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  // Outbound rule for all traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "server" {
  ami = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name = "key"
  security_groups = [aws_security_group.allow_ssh.name]

  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 1
  }

  provisioner "local-exec" {
    command = "puppet apply D:/worldline/assignment/assignment4/puppet/site.pp"
  }
}

