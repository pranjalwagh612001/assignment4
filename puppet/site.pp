node 'default' {
  package { 'nginx':
    ensure => 'present',
    source => 'C:\nginx-1.24.0\nginx.exe'
  }

  service { 'nginx':
    ensure => 'running',
    enable => true,
    provider => 'windows'
  }
}
